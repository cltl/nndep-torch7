An implementation of Stanford neural network dependency parser (Chen & Manning, 2014) in Lua/Torch7.

## Prepare data

1. Download word cluster from [Stanford POS tagger website](http://nlp.stanford.edu/software/pos-tagger-faq.shtml#distsim) and put into folder `stanford-postagger-2015-12-09`
2. Download [PENN Treebank 3](https://catalog.ldc.upenn.edu/LDC99T42) and put the WSJ part into folder `penntree` (only data files, not instructions, license, etc.)
3. Run `th dep/prepare.lua` (takes about a day to finish)

## Run experiments

1. Run original parser + published models on standard test sets: `th dep/stanford-published-model.lua`
2. Train and test original parser on standard split of data: `th dep/stanford-training.lua`
3. Run re-implemented parser + published models on standard test sets: `th dep/parse-published-model.lua`
4. Train and test re-implemented parser on standard split of data: `th dep/exp_chen_manning_2014.lua --dependency sd` (Stanford dependencies) and `th dep/exp_chen_manning_2014.lua --dependency lth` (CoNLL dependencies). If you have access to a CUDA-enabled GPU, use `--cuda`. Each script takes about 1.5 hour.
5. Measure statistics of datasets: `th dep/stats.lua`
6. Run all tests (useful for development and maintenance): `th test/test_all.lua`

Some configurations can be altered by changing `dep/config.lua`.

- Turning on NP-bracketing: uncomment the line `data_dir = paths.concat(home_dir, '../penntree.np-bracketing')`
- Turning off POS tag jackknifing: comment out the line `jackknife_dir = paths.concat(out_dir, 'penntree.jackknife')`
- Turning on POS tagger's model caching: since training POS taggers is the most time consuming preprocessing step, turning this option on can save you some time. 

You will need to clean the output directory by yourself (see `out_dir` in the config file) and rerun `prepare.lua`.

## Tested on

All experiments were carried out in a virtual environment equipped with:

- CPU: Haswell, no TSX
- RAM: 62 GB
- GPUs: GRID K2 (used only 1 of 2 GPUs)

Reference running time was also measured in this environment.

## References

Chen, D., & Manning, C. (2014). A Fast and Accurate Dependency Parser using Neural Networks. In Proceedings of the 2014 Conference on Empirical Methods in Natural Language Processing (EMNLP) (pp. 740–750). Doha, Qatar: Association for Computational Linguistics.